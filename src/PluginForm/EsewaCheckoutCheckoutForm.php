<?php

namespace Drupal\esewa\PluginForm;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Component\Utility\Random;
use Cixware\Esewa\Client;
use Cixware\Esewa\Config;

class EsewaCheckoutCheckoutForm extends BasePaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $configuration = $payment_gateway_plugin->getConfiguration();
    $mode = $configuration['mode'];
    $pass_customer_email = $configuration['pass_customer_email']??false;

    $order = $payment->getOrder();

    $random = new Random();
    $length = 30;

    $stsess = $random->string($length);
    $stsess = substr(bin2hex(random_bytes(8)), 0, 16);

    /** @var \Drupal\esewa\Plugin\Commerce\PaymentGateway $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();

    $session = \Drupal::service('session');
    $session->set('esewa_payment', $payment);
    $session->set('esewa_order', $order);
    $session->set('stsess', $stsess);

    $esewa_data = $payment_gateway_plugin->setEsewaCheckoutCheckoutData($payment);

    $order = $esewa_data['order'];
    $currency = $esewa_data['currency'];

    if($currency == 'NPR') {
      $line_items_data = array();

      try {
        if (class_exists('\Cixware\Esewa\Client')) {

         $total_amount = 0;

         if (!empty($order)) {

        // Get the total price from the order.
          $total_price = $order->getTotalPrice();

          $order_id = $order->id();

          $total_amount = $total_price->getNumber();

          // $success_redirect_url = Url::fromRoute('esewa.payment_success', ['stsess' => $stsess], ['absolute' => true])->toString();
          // $cancel_redirect_url = Url::fromRoute('esewa.payment_cancel', ['stsess' => $stsess], ['absolute' => true])->toString();

          $success_redirect_url = Url::fromRoute('esewa.payment_success', [], ['absolute' => true])->toString();
          $cancel_redirect_url = Url::fromRoute('esewa.payment_cancel', [], ['absolute' => true])->toString();

          if($mode == 'live') {
            $merchant_code = $configuration['merchant_code']??'';

          // Config for production.
            $config = new Config($success_redirect_url, $cancel_redirect_url, $merchant_code);
          } else {
          // Config for development.
            $config = new Config($success_redirect_url, $cancel_redirect_url);

          }

        // Initialize eSewa client.
          $esewa = new Client($config);

        // Generate random 16 characters product ID
          $productId = substr(bin2hex(random_bytes(8)), 0, 16);

          $tax_amount = 0;

        // Process the payment
          $esewa->process($stsess, $total_amount, $tax_amount);
        }
      } else {
        $message = 'Something went wrong. Please contact the site administrator.';
        \Drupal::messenger()->addError(t($message));

        $message = 'eSewa is missing on the system. Please install the dependency with following command "composer require cixware/esewa-php-sdk ^2.5';
        log_esewa_messages('alert', $message);
      }
    } catch (\Exception $e) {
      $message = $e->getMessage();
      \Drupal::messenger()->addError(t($message));
      log_esewa_messages('alert', $message);
    }
  } else {
    $message = 'Please contact the site administrator. eSewa supports payments in Nepalese Rupees (NPR) only.';
    \Drupal::messenger()->addError(t($message));
  }
}
}
