<?php

namespace Drupal\esewa\Event;

/**
 * Defines events for the Commerce CommerceEsewaCheckout module.
 */
final class CommerceEsewaCheckoutEvents {

  /*
    * Name of the event fired when CommerceEsewaCheckout authorizes a transaction.
    *
    * @Event
    *
    * @see \Drupal\esewa\Event\CommerceEsewaCheckoutPaymentEvent
    */
  const PAYMENT_SUCCESS = 'esewa.payment_success';
  /**
   * Name of the event fired when CommerceEsewaCheckout voids a transaction.
   *
   * @Event
   *
   * @see \Drupal\esewa\Event\CommerceEsewaCheckoutPaymentEvent
   */
  const PAYMENT_FAILURE = 'esewa.payment_failure';

}