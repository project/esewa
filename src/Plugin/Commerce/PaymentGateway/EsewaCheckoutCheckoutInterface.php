<?php

namespace Drupal\esewa\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Entity\PaymentInterface;

/**
 * Provides the interface for the CommerceEsewaCheckout Checkout payment gateway.
 */
interface EsewaCheckoutCheckoutInterface {
  
  /**
   * SetEsewaCheckoutCheckout request.
   *
   * Builds the data for the request.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment.
   *
   * @return array
   *   CommerceEsewaCheckout data.
   *   
   */
  public function setEsewaCheckoutCheckoutData(PaymentInterface $payment);
}
