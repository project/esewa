<?php

namespace Drupal\esewa\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;

/**
 * Defines EsewaController class.
 */
class EsewaController extends ControllerBase
{
    /**
     * Callback URL handling for Sendinblue API API.
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     *   Request.
     *
     * @return array
     *   Return markup for the page.
     */
    public function payment_success()
    {
        $pid = \Drupal::request()->query->get('pid');
        $get_esewa_stsess = $this->get_esewa_stsess();

        if ($pid != $get_esewa_stsess) {
            return $this->redirect_homepage();
        }

        $get_esewa_payment = $this->get_esewa_payment();
        $get_esewa_order = $this->get_esewa_order();

        if ($get_esewa_order instanceof OrderInterface && $get_esewa_payment instanceof PaymentInterface) {

            esewa_change_payment_status_completed($get_esewa_payment, $get_esewa_order);

            $message = '';
            // Invoke the custom hook for success message.
            \Drupal::moduleHandler()->invokeAll('esewa_success_message', [&$message]);
            if($message == '') {
                $message = '<strong>Success!</strong> Your payment has been successfully processed.';
            }

            \Drupal::messenger()->addMessage($this->t($message));

            $this->unset_esewa_session();

            $url = Url::fromRoute('commerce_checkout.form', ['commerce_order' => $get_esewa_order->id()]);

            $response = new RedirectResponse($url->toString());
            // $response->send();
            return $response;
        } else {
            return $this->redirect_homepage();
        }
    }

    /**
     * Callback URL handling for Sendinblue API API.
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     *   Request.
     *
     * @return array
     *   Return markup for the page.
     */
    public function payment_cancel()
    {
        $pid = \Drupal::request()->query->get('pid');
        $get_esewa_stsess = $this->get_esewa_stsess();

        if ($pid != $get_esewa_stsess) {
            return $this->redirect_homepage();
        }

        $get_esewa_order = $this->get_esewa_order();

        if ($get_esewa_order instanceof OrderInterface) {
            esewa_change_payment_status_cancelled($get_esewa_order);
            $this->unset_esewa_session();

            $message = '';

            // Invoke the custom hook for failure message.
            \Drupal::moduleHandler()->invokeAll('esewa_failure_message', [&$message]);

            if($message == '') {
                $message =  '<strong>Payment Canceled!</strong> Your transaction has been canceled.';
            }

            \Drupal::messenger()->addMessage($this->t($message));
        }

        return $this->redirect_homepage();
    }

    /**
     * get esewa payment
     */
    public function get_esewa_payment()
    {
        $session = \Drupal::service('session');
        $esewa_payment = $session->get('esewa_payment')??0;
        return $esewa_payment;
    }

    /**
     * Get esewa order
     */
    public function get_esewa_order()
    {
        $session = \Drupal::service('session');
        $esewa_order = $session->get('esewa_order')??0;
        return $esewa_order;
    }

    /**
     * Get esewa session key
     */
    public function get_esewa_stsess()
    {
        $session = \Drupal::service('session');
        $stsess = $session->get('stsess');
        return $stsess;
    }

    /**
     * Unset esewa session
     */
    public function unset_esewa_session()
    {
        $session = \Drupal::service('session');
        $session->set('esewa_payment', []);
        $session->set('esewa_order', []);
        $session->set('stsess', '');
    }

    /**
     * Redirect to homepage
     */
    public function redirect_homepage()
    {
        $redirect_url = Url::fromRoute('<front>', [], ['absolute' => true])->toString();
        $response = new RedirectResponse($redirect_url);
        // $response->send();
        return $response;
    }
}
